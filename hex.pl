use strict;
use warnings;
use utf8;
use Encode qw/encode decode/;
use XML::Simple;
use Data::Dumper "Dumper";
use File::Spec;
use File::Basename qw(basename dirname);
use FindBin;
use lib "$FindBin::Bin/../package";
use UtilLib;
use File::Path "mkpath";
use Encode::Locale;



#設定ファイル読み込み
my $conf_file = shift || "config.xml";
if(!(-f $conf_file)){
	die "Can't find config file";
}
my $xml = XML::Simple->new;
my $conf = $xml->XMLin($conf_file);
my $list = $conf->{list}->{path};
open(my $fh_list, "<", $list) or die(qq{Can't open file:"${list}"});

#文字コードリストのファイル読み込み
my $lists = do{local $/ = undef;<$fh_list>};
$lists = decode($conf->{list}->{charset}, $lists);
close($fh_list);

#単語ファイルの読み込み
my $util_lib = UtilLib->new();
my $in_dir = File::Spec->rel2abs($conf->{in}->{dir});
my $out_dir = $conf->{out}->{dir};
my @files = $util_lib->rec_opendir(encode(locale_fs => $in_dir));

foreach my $file(@files){
	next unless $file =~ /\.u.*$/;
	my @valid_lines = ();
	my @invalid_lines = ();

	my @lines = $util_lib->openfile($file, $conf->{in}->{charset});
	foreach my $line (@lines){
		if($line =~ /^;/){
			push(@valid_lines, $line);
			next;
		}
		my @sep = split(/\s+/, $line);
		my $invalid = 0;
		foreach my $ch(split(//, $sep[1])){
			my $unpacked = unpack("H*", encode($conf->{list}->{code_charset}, $ch));
			$unpacked = uc $unpacked;
			if($lists !~ /^0x${unpacked}$/m){
				$invalid = 1;
				last;
			}
		}

		if($invalid == 1){
			push(@invalid_lines, $line);

		}
		else{
			push(@valid_lines, $line);
		}

	}
	my $invalid_file = File::Spec->catfile($out_dir, $util_lib->replace(qr/^\Q${in_dir}\E/, "invalid", decode(locale_fs => $file)));
	my $invalid_dir = dirname($invalid_file);
	mkpath(encode(locale_fs => $invalid_dir));
	$util_lib->writefile(encode(locale_fs => $invalid_file), $conf->{in}->{charset}, \@invalid_lines);

	my $valid_file = File::Spec->catfile($out_dir, $util_lib->replace(qr/^\Q${in_dir}\E/, "valid", decode(locale_fs => $file)));
	my $valid_dir = dirname($valid_file);
	mkpath(encode(locale_fs => $valid_dir));
	$util_lib->writefile(encode(locale_fs => $valid_file), $conf->{in}->{charset}, \@valid_lines);

}

