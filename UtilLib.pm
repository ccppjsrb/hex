package UtilLib;
use strict;
use warnings;
use utf8;
use Encode qw(encode decode);
use File::Spec;

sub new{
	my $class = shift;
	my $self = {
		@_
	};
	return bless $self, $class;
}
sub rec_opendir{
	my ($self, $exp) = @_;
	my @files = ();
	if(-d $exp){
		opendir(my $dh, $exp) or die(qq/Can't open dir $exp/);
		while(my $file = readdir($dh)){
			next if $file eq '.' || $file eq '..';
			my @childs = $self->rec_opendir(File::Spec->catfile($exp, $file));
			push @files, @childs;
		}
		closedir($dh);
	}
	elsif(-f $exp){
		push @files, $exp;
	}
	return @files;
}

sub openfile{
	my ($self, $file, $charset) = @_;
	open(my $fh, "<", $file) or die qq(Can't open file: ${file});
	my $lines = do{local $/ = undef; <$fh>};
	my $decoded = decode($charset, $lines);
	close($fh);
	return split(/\n/, $decoded);
}

sub writefile{
	my ($self, $file, $charset, $datas) = @_;
	open(my $fh, ">", $file) or die qq(Can't open file: ${file});
	binmode($fh);
	foreach my $data(@{$datas}){
		print $fh encode($charset, $data."\n");
	}
	close($fh);
}

sub replace{
	my ($self, $regexp, $replace, $str) = @_;
	$str =~ s/$regexp/$replace/;
	return $str;
}
1;